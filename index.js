// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");

// const path = require("path");

//Import thư viện mongoose
// const mongoose = require("mongoose");

//import models
const conNhanModel = require("./app/models/congNhanModel");
const conNguoiModel = require("./app/models/conNguoiModel");
const hocSinhModel = require("./app/models/hocSinhModel");
const sinhVienModel = require("./app/models/sinhVienModel");
// Khởi tạo app express
const app = express();


// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// app.use(express.json());

app.use((req, res, next) => {
    let today = new Date();

    console.log("Current: ", today);

    next();
})

app.use((req, res, next) => {
    console.log("Method: ", req.method);

    next();
})
 let congNhan = new conNhanModel ("Chung Chí Bảo Son", "15/4/1993", "Tp HCM", "Web dev", "Freelance", "1000$");
 let conNguoi = new conNguoiModel ("Chung Chí Bảo Son", "15/4/1993", "Tp HCM");
 let hocSinh = new hocSinhModel ("Chung Chí Bảo Son", "15/4/1993", "Tp HCM","IRON HACK","R2554", "0785698577");
 let sinhVien = new sinhVienModel ("IRON HACK","R2554", "0785698577","Java full stack","156");

 console.log(congNhan instanceof conNguoiModel);
 console.log(congNhan instanceof hocSinhModel);
 console.log(congNhan instanceof sinhVienModel);
 console.log(conNguoi instanceof conNhanModel);
 console.log(conNguoi instanceof hocSinhModel);
 console.log(conNguoi instanceof sinhVienModel);
 console.log(hocSinh instanceof conNguoiModel);
 console.log(hocSinh instanceof conNhanModel);
 console.log(hocSinh instanceof sinhVienModel);
 console.log(sinhVien instanceof conNhanModel);
 console.log(sinhVien instanceof conNguoiModel);
 console.log(sinhVien instanceof hocSinhModelOpen Folder Context Menus for VS Code);


// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
    console.log("App listening on port: ", port);
});

