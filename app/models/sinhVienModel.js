//Import thư viện express
const express = require("express");

const hocSinhModel = require("../models/hocSinhModel");

class sinhVienModel extends hocSinhModel {
 constructor(tenTruong,lop,soDienThoai,chuyenNganh,MSSV){
  super(tenTruong,lop,soDienThoai)
  this.chuyenNganh = chuyenNganh,
  this.MSSV = MSSV
 }
}

module.exports = sinhVienModel;