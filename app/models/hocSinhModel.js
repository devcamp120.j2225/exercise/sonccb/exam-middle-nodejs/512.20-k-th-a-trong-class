//Import thư viện express
const express = require("express");

const conNguoiModel = require("../models/conNguoiModel");

class hocSinhModel extends conNguoiModel {
 constructor(hoTen,ngaySinh,queQuan,tenTruong,lop,soDienThoai){
  super(hoTen,ngaySinh,queQuan)
  this.tenTruong = tenTruong,
  this.lop = lop,
  this.soDienThoai = soDienThoai;
 }
}

module.exports = hocSinhModel;