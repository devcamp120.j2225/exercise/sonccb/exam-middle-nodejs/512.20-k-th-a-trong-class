//Import thư viện express
const express = require("express");

const conNguoiModel = require("../models/conNguoiModel");

class congNhanModel extends conNguoiModel {
 constructor(hoTen,ngaySinh,queQuan,nganhNghe,noiLamViec,luong){
  super(hoTen,ngaySinh,queQuan)
  this.nganhNghe = nganhNghe,
  this.noiLamViec = noiLamViec,
  this.luong = luong;
 }
}

module.exports = congNhanModel;